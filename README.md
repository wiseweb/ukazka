# README #
This is a sample repository to see some code, which I made.

# PHP #
There is a folder ./php which is an excerpt from api, I've created and currently using to simplify some tasks. This is not working. Live version is on http://podiobridge.vitaverde.sk/api/ You can try it by adding ping comm

# HTML/JS/CSS #
Another folder ./valentin contains an app which was used to promote services during valentin 2016. It is mainly html, js, css and is working. You can find it live on [website](https://vitaverde.sk/valentin) which I currently take care of.

# Live sites (notable) #
* http://masaze-nitra.eu
* http://kozmetika-nitra.sk
* https://vitaverde.sk
* https://shop.vitaverde.sk
* https://blog.vitaverde.sk