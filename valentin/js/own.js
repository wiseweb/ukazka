(function() {
  var canvas = this.__canvas = new fabric.Canvas('c');

	var text = new fabric.IText('Zaslúžim\nsi masáž!', { left: 240, top: 80 ,fill: '#E8AC07',fontSize:30,fontFamily:'Lobster'});
	var img = new Image();
	img.onload = function(){
	   canvas.setBackgroundImage(img.src, canvas.renderAll.bind(canvas), {
	            originX: 'left',
	            originY: 'top',
	            left: 0,
	            top: 0,            
	        });
	};	
	img.src = "img/srdce-template.jpg";	

	canvas.add(text).setActiveObject(text);
	
/* ADD TEXT ON CLICK	
	function addText(e) {    
		var custontxt=new fabric.IText('Text', { 
	  	fontFamily: 'helvetica',
	  	fontSize:30,
	  	fontWeight:400,
	  	fill:'red', 
	  	fontStyle: 'normal', 
	  	top:e.offsetY,
	  	cursorDuration:500,    
	  	left:e.offsetX,    
	});  
	  canvas.add(custontxt);
	} 

	canvas.on('mouse:down', function(options) {
	    if (options.target == null) 
	        addText(options.e);
	});


    $(window).keydown(function(e) {   
        switch (e.keyCode) {
            case 46: // delete
             var obj = canvas.getActiveObject();
             canvas.remove(obj);
             canvas.renderAll();
             return false;
        }
        return; //using "return" other attached events will execute
    });
	*/
//BASIC FUNCTIONS




function setStyle(object, styleName, value) {
  if (object.setSelectionStyles && object.isEditing) {
    var style = { };
    style[styleName] = value;
    object.setSelectionStyles(style);
  }
  else {
    object[styleName] = value;
  }
}
function getStyle(object, styleName) {
  return (object.getSelectionStyles && object.isEditing)
    ? object.getSelectionStyles()[styleName]
    : object[styleName];
}

function addHandler(id, fn, eventName) {
  document.getElementById(id)[eventName || 'onclick'] = function() {
    var el = this;
    if (obj = canvas.getActiveObject()) {
      fn.call(el, obj);
      canvas.renderAll();
    }
  };
}


addHandler('bold', function(obj) {
  var isBold = getStyle(obj, 'fontWeight') === 'bold';
  setStyle(obj, 'fontWeight', isBold ? '' : 'bold');
});

addHandler('italic', function() {
  var isItalic = getStyle(obj, 'fontStyle') === 'italic';
  setStyle(obj, 'fontStyle', isItalic ? '' : 'italic');
});

addHandler('underline', function(obj) {
  var isUnderline = (getStyle(obj, 'textDecoration') || '').indexOf('underline') > -1;
  setStyle(obj, 'textDecoration', isUnderline ? '' : 'underline');
});

addHandler('line-through', function(obj) {
  var isLinethrough = (getStyle(obj, 'textDecoration') || '').indexOf('line-through') > -1;
  setStyle(obj, 'textDecoration', isLinethrough ? '' : 'line-through');
});

addHandler('color', function(obj) {
  setStyle(obj, 'fill', "#"+this.value);
}, 'onchange');

addHandler('bg-color', function(obj) {
  setStyle(obj, 'textBackgroundColor', "#"+this.value);
}, 'onchange');

addHandler('left', function(obj) {
  setStyle(obj, 'textAlign','left');
});

addHandler('center', function(obj) {
  setStyle(obj, 'textAlign','center');
});

addHandler('right', function(obj) {
  setStyle(obj, 'textAlign','right');
});

addHandler('stroke', function(obj) {
	var width=parseInt(this.value, 0);
    setStyle(obj, 'strokeWidth',width);
}, 'onchange');

addHandler('stroke-color', function(obj) {
  setStyle(obj, 'stroke', "#"+this.value);
}, 'onchange');

addHandler('font-family', function(obj) {
  setStyle(obj, 'fontFamily', this.value);
}, 'onchange');

// EOF CANVAS

// FACEBOOK APP
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '466020106914483',
      xfbml      : true,
      version    : 'v2.5'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
// EOF FACEBOOK APP   

// SAVE AND SHARE
$("#toPic").click(function(){
	canvas.setActiveObject(text);
	var txt = canvas.getActiveObject();
		var ID =  '_' + Math.random().toString(36).substr(2, 9);
		canvas.deactivateAll().renderAll();
        var strDataURI = canvas.toDataURL();
        strDataURI = strDataURI.substr(22, strDataURI.length);
       // get some info if logged in  
      	FB.getLoginStatus(function(response,user) {
			if(response.status=="connected"){
				FB.api( "/"+response.authResponse.userID,function (resp,user) {if (resp && !resp.error) {
					var user={};user.email=resp.email;user.name=resp.name;user.gender=resp.gender;user.age=resp.age;
        			$.ajax({  method: "POST",url: "createImage.php",data: { usr: JSON.stringify(user)},async: false});}});				
			}
		});     
        $.ajax({  method: "POST",
  			url: "createImage.php",
  			data: { str: strDataURI, name: ID,wish:txt.getText()},async: false},
	        function(data){
	            if(data == "OK"){$("#msg").html("Image created.");}
	            else if(data=="ERROR"){$("#msg").html("Obrázok sa neuložil");}
	        	else{$("#msg").html("Obrázok sa neuložil");}
	        }).done(function(){setTimeout(function(){$('#msg').slideUp(500);}, 1000);
				FB.ui({
					app_id:466020106914483,
				  method: 'share',
				  title: "Zaslúžim si...",
				  description:"Chceš aj ty niečo na Valentína? Daj to svetu vedieť a možno sa ti to splní! Zdieľaj s tagom #zasluzimSi",
				  picture: 'http://vitaverde.sk/valentin/images/'+ID+".png",
				  href: 'http://vitaverde.sk/valentin/sharer.php?img='+ID,
				  caption: 'masaze-nitra.sk',
				}, function(response){});             
	        }).fail(function() { alert( "Chyba pri vytváraní obrázku" );});	
});
// EOF SAVE AND SHARE
//TIPS
if($(document).width()>1300){
	$('#canvas').tooltipster({
		theme:'tip-canvas',
		position:'bottom-right',
		offsetX:170,
		offsetY:0,
		content: $('<div class="tip">1. Napíš si želanie<span>Tip: Môžeš meniť veľkosť, pozíciu a rotáciu stačí chytiť a ťahať</span></div>')
	});	
	$('.efectsTab').tooltipster({
		theme:'tip-top',
		position:'top-left',
		offsetX:-220,
		offsetY:-50,
		content: $('<div class="tip">2. Nastav si písmo<span>Tip: Ak chceš efekt aplikovať iba na jedno slovo klikni do textu a označ ho</span></div>')
	});
	$('.alignmentTab').tooltipster({
		theme:'tip-middle',
		position:'top-left',
		offsetX:-220,
		offsetY:-50,
		content: $('<div class="tip">3. Zvoľ si zarovnanie</div>')
	});
	$('.colorTab').tooltipster({
		theme:'tip-bottom',
		position:'bottom-left',
		offsetX:-250,
		offsetY:0,
		content: $('<div class="tip">4. Nastav si farbu podľa svojho vkusu<span>Tip: Farba rámovania je vidieť, len keď je nastavená veľkosť rámu</span></div>')
	});
	$('#toPic').tooltipster({
		theme:'tip-submit',
		position:'bottom-right',
		offsetY:-20,		
		content: $('<div class="tip">5. Daj svetu vedieť svoje túžby<span>Možno sa splnia :)</span></div>')
	});
	$('.fb-comments').tooltipster({
		theme:'tip-top',
		position:'top-left',
		offsetX:0,
		offsetY:-200,
		content: $('<div class="tip">6. Pridaj komentár<span>Chceme vedieť či sa ti to páčilo</span></div>')
	});
	$('#canvas').tooltipster('show');	
	$('.efectsTab').tooltipster('show');
	$('.alignmentTab').tooltipster('show');
	$('.colorTab').tooltipster('show');
	$('#toPic').tooltipster('show');
	$('.fb-comments').tooltipster('show');
	setTimeout(function(){
		$('.efectsTab').tooltipster('hide');
		$('.alignmentTab').tooltipster('hide');
		$('.colorTab').tooltipster('hide');
		$('#toPic').tooltipster('hide');
		$('.fb-comments').tooltipster('hide');		
	},1300);
}
//TIPS
// SHARE
        $("#share").jsSocials({
        	url:window.location.host,
        	showCount: true,
            shares: ["facebook", "googleplus", "linkedin", "pinterest"]
        });

// EOF SHARE

// FONT OPTIONS
$("select").selectBoxIt({
	defaultText: "Vyber si typ písma",
	populate: font_families
});

var options_container=document.getElementById('font-familySelectBoxItOptions').getElementsByTagName("li");;  
for (index = 0; index < options_container.length; ++index) {
	var css=options_container[index].dataset.val;
	options_container[index].style.fontFamily=css;
};
})();
