<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"  "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
    <HEAD>
        <TITLE>Zaslúžim si....</TITLE>
         <meta charset="UTF-8"> 
        <meta property="og:url"                content="<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>" />
        <meta property="og:type"               content="website" />
        <meta property="og:title"              content="Zaslúžim si...." />
        <meta property="og:description"        content="Chceš niečo na valentína? Daj to svetu vedieť a možno sa ti to splní!" />
        <meta property="og:image"              content="<?php echo "http://$_SERVER[HTTP_HOST]"?>/valentin/img/srdce-template.jpg" />    
        <meta property="fb:app_id" content="466020106914483" />    
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
        <link rel="canonical" href="<?php echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"?>" /> 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">               
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js"></script>
        <script>
            var font_families= Array('Open Sans','Alegreya','Work Sans','Playfair Display', 'Droid Serif','Prociono','Playfair Display','League Gothic','Lobster','Amatic SC','Courgette','Great Vibes','Grand Hotel','Coda','Amatic SC','Righteous','Kaushan Script','Great Vibes','Comfortaa','Marck Script','Audiowide','Bubblegum Sans','Alex Brush','Freckle Face');
            var f=Array();
            for(var i=0;i<font_families.length;i++){
                f[i]=font_families[i].replace(' ', '+')+"::latin,latin-ext";
            };
          WebFontConfig = {
            google: { families: f }
          };

          (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
              '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
          })();           
        </script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55616777-2', 'auto');
  ga('send', 'pageview');

</script>        
        <link href="css/jssocials/jssocials.css" rel="stylesheet">
        <link href="css/jssocials/jssocials-theme-flat.css" rel="stylesheet">           
        <link rel="stylesheet" href="css/selectBoxIt.css" />      
        <link rel="stylesheet" type="text/css" href="css/tooltipster.css" /> 
        <link rel="stylesheet" href="css/style.css" />        
    </HEAD>
    <BODY>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=114849405230549";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>        
<div class="container">        
    <div class="row">
        <div class="col-md-12" style="text-align: center;font-family:'Grand Hotel';font-size:2.5em;margin-top:20px">Vytvor si originálne prianie na Valentína, a pošli ho všetkým koho máš rád/rada.</div>
    </div>
    <div style="height:60px;"> </div>
    <div class="row">        
        <div class="col-md-4 col-md-offset-1 controls">
            <div class="form-group">            
                <select id="font-family"></select>  
            </div>          
            <a class="btn btn-primary btn-lg btn-block panel-toggle efectsTab" data-toggle="collapse" data-target="#efectsTab" onclick="ga('send', 'event', 'controls', 'click', 'fontEffects')">Textové efekty</a>
            <div id="efectsTab" class="panel-collapse collapse" >
                <div class="btn-group" role="group" aria-label="Text">
                    <button id="bold" class="btn">B</button>
                    <button id="italic" class="btn">I</button>
                    <button id="underline" class="btn">Podčiarknuté</button>
                    <button id="line-through" class="btn">Prečiarknuté</button>
                </div>
            </div>
            <a  class="btn btn-primary btn-lg btn-block panel-toggle alignmentTab"  data-toggle="collapse" data-target="#alignmentTab" onclick="ga('send', 'event', 'controls', 'click', 'alignment')">Zarovnanie</a>
            <div id="alignmentTab" class="panel-collapse collapse" >            
                <div class="btn-group" role="group" aria-label="Zarovnanie">        
                    <button id="left" class="btn glyphicon glyphicon-align-left"></button>
                    <button id="center" class="btn glyphicon glyphicon-align-center"></button>
                    <button id="right" class="btn glyphicon glyphicon-align-right"></button>
                </div>
            </div>
            <a  class="btn btn-primary btn-lg btn-block panel-toggle colorTab"  data-toggle="collapse" data-target="#colorTab" onclick="ga('send', 'event', 'controls', 'click', 'color')">Farba</a>
            <div id="colorTab" class="panel-collapse collapse" >            
                <div class="btn-group row" role="group" aria-label="Farba">       
                    <div class="form-group col-md-6">            
                        <label for="color">Písmo</label>                     
                        <input id="color" class="jscolor" value="000000">
                    </div>
                    <div class="form-group col-md-6">            
                        <label for="bg-color">Pozadie</label>                     
                        <input id="bg-color" class="jscolor" value="FFFFFF">
                    </div>
                    <div class="form-group col-md-6">            
                        <label for="stroke-color">Rámovanie</label>     
                        <input id="stroke-color" class="jscolor" value="FFFFFF"> 
                    </div>   
                    <div class="form-group col-md-6">            
                        <label for="stroke">Veľkosť rámovania</label>                                                               
                        <input min="0" max="10" value="0" id="stroke" type="range">
                    </div>

                </div>
            </div>  
            <button id="toPic" class="btn btn-success btn-lg btn-block" onclick="ga('send', 'event', 'controls', 'click', 'save')">Uložiť &amp; zdieľať</button>           
             <div id="share"></div>                                   
        </div>
        <div class="col-md-6" id="canvas">       	
            <canvas id="c" width="600" height="313" style="border:1px solid #000000;"></canvas>
            <div id="msg"></div>
        </div>
    </div>
    <div style="height:90px"></div>
    <div class="fb-comments" data-href="https://vitaverde.sk/valentin/" data-numposts="5"></div>    
    </div>    
    
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>    
    <script type="text/javascript" src="js/fabric.min.js"></script>
    <script type="text/javascript" src="js/jscolor.min.js"></script>
    <script type="text/javascript" src="js/jquery.tooltipster.min.js"></script>
    <script type="text/javascript" src="js/selectBoxIt.min.js"></script>
    <script src="js/jssocials.min.js"></script>    
    <script type="text/javascript" src="js/own.js"></script>

    </BODY>
</HTML>