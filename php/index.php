<?php
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
session_start();
require_once(__DIR__.'/defines.php');
require_once(PATH.DS.'loader.php');
use PodioBridge\Classes\Api as rest_api;

try
{
	if(DEBUG){
		ini_set('display_errors', '1');
		if(!is_dir(PATH.DS."logs")) mkdir(PATH.DS."logs");
	}
	$api=new rest_api;
	if($api->load()===false)die("Bad router");
	$api->call();

}
catch(Exception $e) {
	http_response_code(400);
	if(DEBUG) echo 'Caught exception: <pre>',  print_r($e), "</pre>\n";
}
?>