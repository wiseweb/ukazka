<?php
namespace PodioBridge\Classes\Get;
use PodioBridge\Libs\ApiLib as ApiLib;
/**
*
*/
class Facebook extends ApiLib
{
	/*
		<span style="font-size:50px;"><strong><a href="http://podiobridge.vitaverde.sk/api/facebook/shareStory/%type%/%calendar%/%price%">Bol to super zážitok</a></strong></span>
	 */
	public function shareStory(){
		$appt_name=urldecode(array_shift($this->commands));
		$appt_calendar=urldecode(array_shift($this->commands));
		$appt_price=urldecode(array_shift($this->commands));
		$fb=\PodioBridge\loadModel("facebook.facebook");
		if(!$fb->checkLogin()){
			$fb->getJsLoginButton();
			return;
		}
		$acuity=\PodioBridge\loadModel("acuity.acuity");
		$appt_types=$acuity->request('/appointment-types');
		foreach($appt_types as $appt_type){
			if($appt_type["name"]==$appt_name && $appt_type["price"]==$appt_price){
				$appt_type['link']="https://app.acuityscheduling.com/schedule.php?owner=11901240&appointmentType=".$appt_type['id'];
				break;
			}
		}
		$fb->shareExperience($appt_type);
		header("Location: https://vitaverde.sk/feedback/?star=4&calendar=".$appt_calendar);
	}

}
?>