<?php
namespace PodioBridge\Classes\Get;
use PodioBridge\Libs\ApiLib as ApiLib;

/**
*
*/
class Podio extends ApiLib
{
	/**
	 * Function to unsubscribe person from Podio
	 * @return podio changed value
	 */
	public function odhlas(){
		$usersApp=\PodioBridge\loadModel('podio.users');
		$user_id=array_shift($this->commands);
		$user_id = preg_replace('/\s+/', '', $user_id);
		$user=$usersApp->getItemById((int)$user_id);
		if(!empty($user)){
			unset($user->fields['povolenie-kontaktovat']);
			$user->fields["povolenie-kontaktovat"]=new \PodioCategoryItemField(array("field_id" => 'povolenie-kontaktovat', "values" => 1));
			$status=$user->save($this->options);
			if(!empty($status)) echo "Je nám ľúto, že už nás nechcete počuť :(";
			else echo "Už odhlásené";
		}
		else  echo "Užívateľ sa nenašiel. Kontaktujte nás prosím na 0948 134 909";
	}		
	/**
	 * remove users without phone, email and visit from podio
	 * @return deleted_users.txt csv file in root folder
	 */
	public function clearDatabase(){
		$usersApp=\PodioBridge\loadModel("podio.users");
		$items=$usersApp->getAllItems(array("limit"=>500));
		$x=1;
		$ids=array();
		foreach($items as $item){
			if(empty($item->fields["phone"]->values) && empty($item->fields["email"]->values) && empty($item->fields["posledna-navsteva"]->values)){
				$ids[]=$item->item_id;
				unset($item->fields['role']);
				$item->fields['role']=new \PodioCategoryItemField(array("field_id" => 'role', "values" => 7));
				$item->save($this->options);
				$this->log("deleted_users",$x.";".$item->fields["first-name"]->values.";".$item->fields["last-name"]->values.";");				
				$x++;
			}
		}
		return $this->response(empty($ids)? "Nothing to delete" :"Archived records: ".implode(";",$ids));
/*	Used to actually delete records from database	
		$options=$this->options;
		$options["item_ids"]=$ids;
		return $this->response($usersApp->bulkDelete($options));
*/	}	
}
?>
