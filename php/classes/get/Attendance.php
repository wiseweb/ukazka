<?php
namespace PodioBridge\Classes\Get;
use PodioBridge\Libs\ApiLib as ApiLib;
/**
*
*/
class Attendance extends ApiLib
{
	/**
	 * shows attendance error repair form
	 * @return \PodioBridge\Libs\response
	 */
	public function repair(){
		$getChkStr=array_shift($this->commands);
		$data=array_shift($this->commands);
		$data=explode(':',$data);
		$dateIds=json_decode(base64_decode($data[0]));
		$d = $this->date();
		$chkstr=md5("VitaVerde".$d->format('mY')."gl8yDVG9tt");
		if($getChkStr!=$chkstr){
			return $this->response("Wrong string",false,NULL,0,$code=401);
		}
		
		$attdModel=\PodioBridge\loadModel('attendance.attendance');
		$d->modify("-1 month");	
		$dates=$attdModel->getDatesByUserIdAndRange($data[1],$d->format('Y-m-01 00:00:00'),$d->format('Y-m-t 23:59:59'));
		//print_r($dateIds);
		$link="http://$_SERVER[HTTP_HOST]/api/attendance/saveRepair/".$chkstr;
		?>
		<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"  "http://www.w3.org/TR/html4/strict.dtd"><HTML><HEAD><TITLE>Oprava</TITLE><meta charset="UTF-8"><link rel="stylesheet" type="text/css" href="style.css"></head><body>		
		<?php
		echo "<form action='".$link."' method='post' style='margin:50px auto;width:300px'>";
		foreach($dates as $key=>$date){
			if(in_array($date['id'],$dateIds)){
				if($date['date']!=@$dates[$key+1]['date']) { // check
					$dt=$this->date($date['datetime']);
					echo $dt->format("d.m.Y")." ";
					$name='d['.$date['id'].']';
					echo $this->createSelect($name.'[sh]',7, 21,$dt->format("h")).":".$this->createSelect($name.'[sm]',0, 60,$dt->format("i"));
					echo ' - '.$this->createSelect($name.'[eh]',7, 21).":".$this->createSelect($name.'[em]',0, 60).'<br>';
				}			
			}
		}
		echo "<input type='hidden' name='user_id' value='".$data[1]."'><div style='text-align:center'><input type='submit' value='Odoslať' style='padding:10px;margin:30px auto;'></div></form>";
		?>
		</body></HTML>
		<?php		
		return $this->response();
	}
	/**
	 * creates select box
	 * @param string $name
	 * @param int $start
	 * @param int $end
	 * @param int $selected
	 * @return string
	 */
	private function createSelect($name,$start,$end,$selected=null){
		$html='<select name="'.$name.'">';
		for($x=$start;$x<=$end;$x++){
			$html.='<option value="'.$x.'"';
			if($x==(int)$selected) $html.=' selected="selected"';
			$html.='>'.sprintf('%02d', $x).'</option>';
		}
		$html.='</select>';
		return $html;
	}
	/**
	 * send attendance to published addresses. Can add var to send to specific address
	 * @return \PodioBridge\Libs\response
	 */
	public function sendAttendance(){
		$getChkStr=array_shift($this->commands);
		if($getChkStr!='gl8yDVG9tt') return $this->response("Wrong string",false,NULL,0,$code=401);
		$attdModel=\PodioBridge\loadModel('attendance.attendance');
		$holidaysModel=\PodioBridge\loadModel('podio.holidays');
		$d = $this->date();
		$d->modify("-1 month");
		$holidays=$holidaysModel->filterItems(array(80165423=>array(2),'date'=>array( "from" => $d->format("Y-m-01 00:00:00"), "to" => $d->format("Y-m-t 23:59:59"))));		
		$users=$attdModel->GetAll("SELECT u.*,max(p.until_date) AS until_date
			FROM users AS u
			LEFT JOIN payments AS p ON u.id=p.user_id
			WHERE u.published=1
			GROUP BY u.id");
		ob_start();
		foreach($users as $user){
			$sql="SELECT *,DATE_FORMAT(datetime,'%d%m%Y') AS date FROM dates WHERE user_id=".$user["id"];
			$sql.=" AND datetime > '".$d->format('Y-m-01 00:00:00')."' AND datetime <'".$d->format( 'Y-m-t 23:59:59' )."'";
			$sql.=" ORDER BY datetime ASC";
			$rows= $attdModel->GetAll($sql);
			echo "<div>Dochádzka pre: <strong>".$user['name']." ".$user['surname']."</strong></div>";
			if(!empty($rows)){
				$total= $d = $this->date('00:00:00');
				$ref = clone $total;
				echo '<table style="width:500px;margin-bottom:30px"><tr><th style="text-align:left">Dátum</th><th style="text-align:left">Príchod</th><th style="text-align:left">Odchod</th></tr>';
				$waysIn=(array_keys(array_filter($rows, function($item){ return $item['way'] === '0';}))); //we need only ways in
				foreach($waysIn as $dateID){
					$error='';
					$dateTime=$this->date($rows[$dateID]['datetime']);
					if(@$rows[$dateID+1]["way"]!="1" || $rows[$dateID]['date']!=$rows[$dateID+1]['date']){$out='-';$error=true;unset($rows[$dateID]);} //checks if way in has a pair out
					else{
						$nextDateTime=$this->date($rows[$dateID+1]['datetime']);
						$difference=date_diff($nextDateTime, $dateTime);
						if($difference->h>15) $error=true; // the difference is too big. No one stays 15+ hrs in work
						$total=date_add($total, $difference);
						$out=$nextDateTime->format("H:i:s");
						$hours = $total->diff($ref)->h+($total->diff($ref)->days*24);
						$minutes=(round($total->diff($ref)->i/60,2));
						$time=$hours+$minutes;
						$dates[]=array("from_date"=>$dateTime,"until_date"=>$nextDateTime,"time"=>$time);
						unset($rows[$dateID],$rows[$dateID+1]);
					}
					if(!$error) echo '<tr>';
					else echo '<tr style="color:red;font-weight:bold">';
					echo "<td>".$dateTime->format("d.m.Y")."</td><td>".$dateTime->format("H:i:s")."</td><td>".$out."</td></tr>";
				}
		
				if(empty($dates))
					echo "</table>";
					else{
						$post_obj=new \stdClass();
						$post_obj->dates=$dates;
						$post_obj->time=$post_obj->dates[count($post_obj->dates)-1]['time'];
						?><tr style="border-top:1px solid #CCC"><td colspan="2">Spolu</td><td><strong><?php echo $post_obj->time?></strong> hodín</td></tr></table><?php
				}
				if(!empty($rows)){
					echo '<div>Chybné dátumy</div><ul>';
					foreach ($rows as $row){
						$dateTime=$this->date($row['datetime']);
						echo '<li>'.$dateTime->format("d.m.Y H:i:s").' - '.(($row['way']==1)? 'odchod':'príchod').'</li>';
					}
					echo '</ul>';
				}
			}
			else{echo "<h1>Žiadne nevyplatené dni nenájdené</h1>";}	
			foreach($holidays as $holiday){
				if($holiday->fields['requester']->values[0]->mail[0]==$user['email']){
					$start=$holiday->fields['date']->values['start'];
					$format=($start->format('His')=="000000")?'d.m.Y':'d.m.Y H:i';
					$start->setTimezone(new \DateTimeZone('Europe/Bratislava'));
					$string= $start->format($format);
					$end=$holiday->fields['date']->values['end'];
					if(!empty($end)){
						$format=($end->format('His')=="000000")?'d.m.Y':'d.m.Y H:i';
						$end->setTimezone(new \DateTimeZone('Europe/Bratislava'));
						$string.= ' - '.$end->format($format);
					}
					
					echo '<p>'.$holiday->fields['type']->values[0]['text'].': '.$string.'</p>';
				}
			}
			echo '<hr><br>';
		}
		$body=ob_get_clean();
		$mailModel=\PodioBridge\loadModel('mail.mail');
		$mail=$mailModel->getMail();
		$mail->setFrom('partneri@vitaverde.sk', 'Vita Verde');
		$recipients=  $attdModel->GetAll("SELECT email FROM emails WHERE published=1"); 
		
		foreach($recipients as $addr){
			$mail->addAddress($addr['email']);     // Add a recipient
		}
		$mail->Subject = 'Dochadzka - '.$d->format('m-Y');
		$mail->Body    =$body;
		if(!$mail->send()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			echo 'Message has been sent';
		}

		return $this->response();
	}
	/**
	 * send email to employees, so they can repair attendance before sending to accountant
	 * @return \PodioBridge\Libs\response
	 */
	public function notifyEmployees(){
		$getChkStr=array_shift($this->commands);
		if($getChkStr!='gl8yDVG9tt') return $this->response("Wrong string",false,NULL,0,$code=401);
		$attdModel=\PodioBridge\loadModel('attendance.attendance');
		$mailModel=\PodioBridge\loadModel('mail.mail');
		$mail=$mailModel->getMail();
		$users=$attdModel->GetAll("SELECT u.*,max(p.until_date) AS until_date
			FROM users AS u
			LEFT JOIN payments AS p ON u.id=p.user_id
			WHERE u.published=1
			GROUP BY u.id");
		$admin_errors=array();
		foreach($users as $user){
			$errors=array();
			$d = $this->date();
			$d->modify("-1 month");
			$sql="SELECT id,user_id,way,DATE_FORMAT(datetime,'%d%m%Y') AS date FROM dates WHERE user_id=".$user["id"];
			$sql.=" AND datetime > '".$d->format('Y-m-01 00:00:00')."' AND datetime <'".$d->format( 'Y-m-t 23:59:59' )."'";
			$sql.=" ORDER BY datetime ASC";
			$rows= $attdModel->GetAll($sql);
			if(!empty($rows)){
				$waysIn=(array_keys(array_filter($rows, function($item){ return $item['way'] === '0';})));
				foreach($waysIn as $dateID){
					if(@$rows[$dateID+1]["way"]!="1" || $rows[$dateID]['date']!=$rows[$dateID+1]['date']){$errors[]=$rows[$dateID]["id"]; unset($rows[$dateID]);}
					elseif(@$rows[$dateID+1]["way"]=="1"){
						unset($rows[$dateID],$rows[$dateID+1]);
					}
				}
			}
			if(!empty($rows)){$admin_errors[$user['surname']]=$rows;}
			if(!empty($errors)){
				$string=rtrim(base64_encode(json_encode($errors)),"=").':'.$user["id"];
				$d =$this->date();
				$chkstr=md5("VitaVerde".$d->format('mY')."gl8yDVG9tt");
				$link="http://$_SERVER[HTTP_HOST]/api/attendance/repair/".$chkstr."/".$string;
				$body="<p>Klinkutím na nasledovný odkaz opravíte chyby v dochádzke. Opravte ich prosím do <strong>".$d->format('04.m.Y')."</strong>. V tento dátum budú odoslané na zaúčtovanie.</p>
				<p><strong><a href='".$link."'>Opraviť chyby</a></strong></p>";
		
				$mail->setFrom('partneri@vitaverde.sk', 'Vita Verde');
				$mail->addAddress($user['email']);     // Add a recipient
				$mail->Subject = 'Chyba v dochadzke - '.$d->format('m-Y');
				$mail->Body    =$body;
				if(!$mail->send()) {
					$admin_errors[$user['surname'].'-send']=array("Neodoslaný email");
				}
			}
		}
		if(!empty($admin_errors)){
			$body= '<div>Chybné dátumy:</div>';
			foreach($admin_errors as $name=>$rows){
				$body.='<p><strong>'.ucfirst($name).'</strong></p><ul>';
				foreach ($rows as $row){
					$dateTime=$this->date($row['datetime']);
					$body.= '<li>'.$dateTime->format("d.m.Y H:i:s").' - '.(($row['way']==1)? 'odchod':'príchod').'</li>';
				}
				$body.= '</ul>';
			}
			// send email to admin if error
			$mail->setFrom('partneri@vitaverde.sk', 'Vita Verde');
			$mail->addAddress('alulord@gmail.com');     // Add a recipient
			$mail->Subject = 'Chyba v dochadzke - '.$d->format('m-Y');
			$mail->Body    =$body;
			if(!$mail->send()) {
				return $this->response($mail->ErrorInfo,false);
			} else {
				return $this->response("Email sent");
			}
		}
		return $this->response("Done");
	}
}
?>