<?php
namespace PodioBridge\Classes\Get;
use PodioBridge\Libs\Api as ApiLib;
/**
*
*/
class Cron extends ApiLib
{
	/**
	 * see description for individual functions
	 * @return bool
	 */
	public function cron(){
		$inactivityStatus=$this->inactivityNotification();
		$archiveStatus=$this->archiveUsers();
		$bDayStatus=$this->bDayNotification();
		$reminderStatus=$this->sendReminder();
		$sendSmsCampaing=$this->sendSmsCampaign();
		$endCampaignStatus=$this->endCampaigns();
		$error_msg='';
		if(!$inactivityStatus){
			$error_msg.="inactivity;".PHP_EOL;
			$error=true;
		}
		if(!$archiveStatus){
			$error_msg.="archive;".PHP_EOL;
			$error=true;
		}
		if(!$bDayStatus){
			$error_msg.="bDay;".PHP_EOL;
			$error=true;
		}
		if(!$bDayStatus){
			$error_msg.="reminder;".PHP_EOL;
			$error=true;
		}
		if(!$sendSmsCampaing){
			$error_msg.="sendSmsCampaing;".PHP_EOL;
			$error=true;
		}
		if(!$endCampaignStatus){
			$error_msg.="endCampaign;".PHP_EOL;
			$error=true;
		}
		if(!$error)
			return $this->response("Done");
		else{
			$this->sendSMS("Cron errors:".$msg,"0903582826");
			return $this->response($error_msg,false);
		}

	}
	/**
	 * cron function
	 * send sms if we have some campaign(has sms text field,marked to send, is ready, starting)
	 * @return bool
	 */
	private function sendSmsCampaign(){
		$dt=$this->date();
		$dt->modify("+3 day");
		$podioCampaignsApp=\PodioBridge\loadModel("podio.campaigns");
		$campaigns=$podioCampaignsApp->filterItems(array(80174691=>array(5),80174692=>array(7,3),'datum'=>array( "from" => $dt->format("Y-m-d 00:00:00"), "to" => $dt->format("Y-m-d 23:59:59"))));
		$ok=true;
		$sms=$social=false;
		foreach($campaigns as $campaign){
			foreach($campaign->fields["type"]->values as $type){
				if($type["id"]==7) {
					foreach($campaign->fields["odoslane"]->values as $odoslane){
						if($odoslane["id"]==3)$sms=true;
					}
				}
				if($type["id"]==3) {
					foreach($campaign->fields["buffer"]->values as $odoslane){
						if($odoslane["id"]==3)$social=true;
					}
				}
			}
			if($social && !empty($campaign->fields["description"]->values)){
				$bufferApp=\PodioBridge\loadModel("buffer.buffer");
				$data=array();
				$text=$bufferApp->cleanBufferText($campaign->fields["description"]->values);
				$data["text"]=$text;
				$data["top"]=true;
				$media=array();
				if($campaign->fields["link"]->values)
					$media["link"]=$bufferApp->cleanBufferText($campaign->fields["link"]->values[0]->original_url);
				if($campaign->fields["obrazok-na-soc-siete"]->values)
					$media["picture"]=$media["thumbnail"]=$media["photo"]=$this->GetImage($campaign->fields["obrazok-na-soc-siete"]->values[0]->file_id,true,"podio.campaigns");
				if(!empty($media))
					$data["media"]=$media;
				$return=$bufferApp->setBufferPost($data);
				if($return->success){
					unset($campaign->fields['buffer']);
					$campaign->fields['buffer']=new \PodioCategoryItemField(array("field_id" => 'buffer', "values" => 2));
					$campaign->save($this->options);
				}
				else{ $ok=false;  $podioCampaignsApp->commentOnItem($campaign->item_id,  'Chyba pri odosielani do buffer: '.$return->message);}
			}
			if($sms && !empty($campaign->fields['text-do-sms']->values)){
				$podioUsersApp=\PodioBridge\loadModel("podio.users");
				$podioUsersApp->filterAllItems(array('role'=>array( 3,1 ),"povolenie-kontaktovat"=>array(2)));
				$x=0;
				foreach($podioUsersApp->items as $users){
					foreach($users as $user){
						$mobile=$this->sanitizeMobile($user->fields['phone']->values);
						if($this->checkMobile($mobile)){
							$body=$campaign->fields['text-do-sms']->values;
							strlen($body)>100 ? $body= substr($body,0,100)."..":$body=$body;
							$body.=". Nechcete od nas sms? http://odhlas.vitaverde.sk/".$user->id;
							if($this->sendSMS($mobile, $body))   $x++;
							else $ok=false;
							$x++;
						}
					}
				}
				$podioCampaignsApp=\PodioBridge\loadModel("podio.campaigns");
				unset($campaign->fields['odoslane']);
				$campaign->fields['odoslane']=new \PodioCategoryItemField(array("field_id" => 'odoslane', "values" => 2));
				$podioCampaignsApp->commentOnItem($campaign->item_id,  'Akcia zaslaná '.$x.' luďom');
				$campaign->save($this->options);
			}
		}
		return $ok;
	}
	/**
	 * cron function
	 * mark campaing finished after end
	 * @return bool
	 */
	private function endCampaigns(){
		$podioCampaignsApp=\PodioBridge\loadModel("podio.campaigns");
		$campaigns=$podioCampaignsApp->filterItems(array(80174691=>array(5)));
		$ok=true;
		foreach($campaigns as $campaign){
			if($this->date("tomorrow")>$campaign->fields["datum"]->values["end"]){
				unset($campaign->fields['status']);
				$campaign->fields['status']=new \PodioCategoryItemField(array("field_id" => 'status', "values" => 4));
				$campaign->save($this->options);
			}
		}
		return $ok;
	}
	/**
	 * cron function
	 * send sms notification of tomorrow appointment if booked x days ago
	 * @return bool
	 */
	private function sendReminder(){
		$acuity=\PodioBridge\loadModel("acuity.acuity");
		$dt=$this->date('tomorrow');
		$options=array("minDate"=>$dt->format('Y-m-d'),"maxDate"=>$dt->format('Y-m-d'));
		$appointments = $acuity->request('/appointments',$options);
		$ok=true;
		foreach($appointments as $appointment){
	   	$mobile=$this->sanitizeMobile($appointment["phone"]);
		   if($this->checkMobile($mobile) && empty($appointment['email'])){
			   $trans = array("január" => "january", "február" => "february", "marec" => "march", "apríl" => "april", "máj" => "may", "jún" => "jun", "júl" => "july", "august" => "august", "september" => "september", "otóber" => "october", "november" => "november", "december" => "december");
			   $cd=strtr($appointment["dateCreated"],$trans);
			   $dt=$this->date($cd);
			   $dt2=$this->date();
				$interval = $dt->diff($dt2);
				if($interval->days>MINIMUM_DAYS_TO_REMIND){
					$desclen=30;
					$service=$appointment["type"];
					$body="Nezabudnite zajtra o ".$appointment["time"]." ";
					if($desclen && strlen($service)<=$desclen)
						$body .= $service;
					else{
						$part=@substr($service,0, @strpos($service, ' ', $desclen));
						if(empty($part) || !$part) $body.= $service;
						else $body.=$part."..";
						 }
					$body.=". Vase Vita Verde. Ak je to omyl, prosim kontaktujte nas na tel. 0948134909";
					if(!$this->sendSMS($mobile, $body))$ok=false;
				}
		   }
	   }
	   return $ok;
	}
	/**
	 * cron function
	 * send sms to user for b-day with random discount
	 * @return bool
	 */
	private function bDayNotification(){
		$u=array();
		$dt=$this->date();
		$podioUsersApp=\PodioBridge\loadModel("podio.users");
		$podioUsersApp->filterAllItems(array('birthday'=>array( "from" => "1900-01-01 00:00:00", "to" => $dt->format("Y-m-d 23:59:59")),'role'=>array( 1,4 )));
		foreach($podioUsersApp->items as $users){
			 foreach($users as $item){
				 if($dt->format("m-d")==$item->fields['birthday']->start->format("m-d") && !empty($item->fields['phone']->values)){
					$discount=round(rand(20,40), 0)."%";
					foreach($item->fields['role']->values as $role){
						if($role["id"]==4) { $discount=round(rand(30,50), 0)."%"; break;}
					}
					$mobile=$this->sanitizeMobile($item->fields['phone']->values);
					 if($this->checkMobile($mobile)){
						if(DEBUG){
							$u[$item->item_id]['name']=$item->fields['first-name']->values;
							$u[$item->item_id]['surname']=$item->fields['last-name']->values;
							$u[$item->item_id]['email']=$item->fields['email']->values;
							$u[$item->item_id]['phone']=$item->fields['phone']->values;
						}
						$body="Vsetko najlepsie ";
						if(strlen($item->fields['first-name']->values)>=3) $body=$this->sanitizeName($item->fields['first-name']->values).", vsetko najlepsie ";
						$body.="prajeme z Vita Verde a pribalujeme ".$discount." zlavu na vsetky sluzby. Tato zlava je platna 3 dni. Preukazte sa na recepcii touto sms";
						if(DEBUG) $u[$item->item_id]["msg"]=$body;
						if($this->sendSMS($mobile, $body))  $podioUsersApp->commentOnItem($item->item_id, 'Zaslané blahoželanie so zľavou '.$discount);
						else $podioUsersApp->commentOnItem($item->item_id, 'Nepodarilo sa zaslať blahoželanie');
					 }
				 }
			 }
		 }
		if(DEBUG && !empty($u)) $this->log("bDay_sent",print_r($u,true));
		return true;
	}
	/**
	 * cron function
	 * archive user (switch status in podio) and send sms notification if they haven't visit in x days
	 * @return bool
	 */
	private function archiveUsers(){
		$u=array();
		$podioUsersApp=\PodioBridge\loadModel("podio.users");
		$podioUsersApp->filterAllItems(array('posledna-navsteva'=>array( "from" => "-".USER_ARCHIVATION_TIME."dr", "to" => "-".USER_ARCHIVATION_TIME."dr"),'role'=>array( 1 )));
		foreach($podioUsersApp->items as $users){
			foreach ($users as $item) {
				if(DEBUG){
					$u[$item->item_id]['name']=$item->fields['first-name']->values;
					$u[$item->item_id]['surname']=$item->fields['last-name']->values;
					$u[$item->item_id]['email']=$item->fields['email']->values;
					$u[$item->item_id]['phone']=$item->fields['phone']->values;
				}
				unset($item->fields['role']);
				$item->fields['role']=new \PodioCategoryItemField(array("field_id" => 'role', "values" => 7));
				unset($item->fields['povolenie-kontaktovat']);
				$item->fields['povolenie-kontaktovat']=new \PodioCategoryItemField(array("field_id" => 'povolenie-kontaktovat', "values" => 1));
				unset($item->fields['newsletter-status']);
				$item->fields['newsletter-status']=new \PodioCategoryItemField(array("field_id" => 'newsletter-status', "values" => 2));
				$item->save($podioUsersApp->options);
			}
		}
		if(DEBUG && !empty($u)) $this->log("archived_users",print_r($u,true));
		return true;
	}
	/**
	 * cron function
	 * send sms notification if they haven't visit in x days
	 * TODO send email instead of sms if mobile number is missing
	 * @return bool
	 */
	private function inactivityNotification(){
		$u=array();
		$podioUsersApp=\PodioBridge\loadModel("podio.users");
		$podioUsersApp->filterAllItems(array('posledna-navsteva'=>array( "from" => "-".LAST_VISIT_WARNING."dr", "to" => "-".LAST_VISIT_WARNING."dr" ),'role'=>array( 1 )));
		foreach($podioUsersApp->items as $users){
			foreach ($users as $item) {
				if(!empty($item->fields['phone']->values)){
					$fName=$item->fields['first-name']->values;
					$mobile=$this->sanitizeMobile($item->fields['phone']->values);
					if(DEBUG){
						$u[$item->item_id]['name']=$fName;
						$u[$item->item_id]['surname']=$item->fields['last-name']->values;
						$u[$item->item_id]['email']=$item->fields['email']->values;
						$u[$item->item_id]['phone']=$mobile;
					}
					if($this->checkMobile($mobile)){
						$body="ezabudajte na seba ;) https://vitaverde.sk/objednavky Toto je jednorazova notifikacia";
						if(strlen($fName)>3) $body=$this->sanitizeName($fName).", n".$body;
						else $body="N".$body;
						if(DEBUG) $u[$item->item_id]["msg"]=$body;
						if($this->sendSMS($mobile, $body))  $podioUsersApp->commentOnItem($item->item_id, 'Zaslaná '.LAST_VISIT_WARNING.' dňová notifikácia');
						else $podioUsersApp->commentOnItem($item->item_id, 'Nepodarila sa zaslať '.LAST_VISIT_WARNING.' dňová notifikácia');
					}

				}
				else $podioUsersApp->commentOnItem($item->item_id, 'Nebol/a u nás už '.LAST_VISIT_WARNING.' dní, ale nemáme kontakt');
			}
		}
		if(DEBUG && !empty($u)) $this->log("notified_users",print_r($u,true));
		return true;
	}
}
?>