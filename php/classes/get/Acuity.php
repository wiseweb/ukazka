<?php
namespace PodioBridge\Classes\Get;
use PodioBridge\Libs\ApiLib as ApiLib;
/**
*
*/
class Acuity extends ApiLib
{
	/**
	 * get appointment statistics for last 3 months
	 * @return statistics.txt csv file in root folder
	 */
	public function getStatistics(){
		$AcuityApp=\PodioBridge\loadModel("acuity.acuity");
		$dt=$this->date();
		$dt->modify("last day of previous month");
		$dt2=clone $dt;
		$dt2->modify("-3 month");
		$options=array("max"=>1000,"minDate"=>$dt2->format('Y-m-d'),"maxDate"=>$dt->format('Y-m-d'));
		$appointments = $AcuityApp->request('/appointments',$options);
		$data=array();
		$types=array("names"=>"firstName","price"=>"price","type"=>"type","calendar"=>"calendar","duration"=>"duration");
		foreach($appointments as $appointment){
			foreach($types as $type=>$name){
				if(@key_exists($appointment[$name],$data[$type])) $data[$type][$appointment[$name]]=$data[$type][$appointment[$name]]+1;
				else $data[$type][$appointment[$name]]=1;
			}
			$date=$this->date($appointment["datetime"]);
			if(@key_exists($date->format("D"),$data["day"])) $data["day"][$date->format("D")]=$data["day"][$date->format("D")]+1;
			else $data["day"][$date->format("D")]=1;
			if(@key_exists($date->format("H"),$data["hour"])) $data["hour"][$date->format("H")]=$data["hour"][$date->format("H")]+1;
			else $data["hour"][$date->format("H")]=1;

		}
		foreach($data as $d=>$dataType){
			file_put_contents("statistics.txt", PHP_EOL.$d.PHP_EOL,FILE_APPEND);
			foreach($dataType as $dataName=>$dataValue){
				file_put_contents("statistics.txt", $dataName.";".$dataValue.PHP_EOL,FILE_APPEND);
			}
		}
		return $this->response("Done");
	}

}
?>