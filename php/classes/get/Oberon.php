<?php
namespace PodioBridge\Classes\Get;
use PodioBridge\Libs\ApiLib as ApiLib;
/**
*
*/
class Oberon extends ApiLib
{
	/**
	 * check if user exists in Podio database
	 * @param string $user_email
	 * TODO check return function, check if used
	 */
	public function CheckExistingUser($user_email=NULL){
		$url=$_SERVER['REQUEST_URI'];
		if(empty($user_email)) $user_email = array_pop($this->commands);
		$app=\PodioBridge\loadModel("podio.users");
		if(!$app || empty($user_email) || !filter_var($user_email, FILTER_VALIDATE_EMAIL))
			return $this->response("Internal error",false);
		$filters=array("email"=>$user_email);
		$items=$app->filterItems($filters);
		if($items->filtered==1)
			return $this->response("OK");
		if($items->filter==0)
			return $this->response("Not found");
		if($items->filter>0)
			return $this->response("Found multiple");
	}
	/**
	 * logout oberon
	 * TODO logout only users with specific id
	 */
	public function Logout(){
		// id to be loged out
		// $this->commands[0];
		if(@unlink("./id"))
			$response=$this->oberonResponse(NULL,true,"Successfully loged out");
		else
			$response=$this->oberonResponse(NULL,true,"Couldn't log out");
		return $response;
	}
	/**
	 * oberon time
	 */
	public function GetDateTime(){
		return $this->oberonResponse(date("Y-m-dTH:i:s"),true,"Dátum a čas odoslaný");
	}
	/**
	 * used in oberon loyalty card form
	 */
	public function GetLoyaltyCardTypes(){
		$cardTypes=array(
		0=>array("ID"=>"1","Name"=>"Klient"),
		1=>array("ID"=>"2","Name"=>"VIP"),
		);
		return $this->oberonResponse($cardTypes,true,"Dátum a čas odoslaný");
	}	
}
?>