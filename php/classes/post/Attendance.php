<?php
namespace PodioBridge\Classes\Post;
use PodioBridge\Libs\ApiLib as ApiLib;
/**
*
*/
class Attendance extends ApiLib
{
	/**
	 * saves outgoing time, updates incoming time if necessary
	 * @return \PodioBridge\Libs\response
	 */
	public function saveRepair(){
		$raw_data=file_get_contents('php://input');
		parse_str($raw_data);
		$attdModel=\PodioBridge\loadModel('attendance.attendance');
		$dates=$attdModel->getDatesById(array_keys($d));
		$errors=array();
		foreach($dates as $date){
			if($user_id!=$date['user_id']) return $this->response('Zlé ID užívateľa',false);
			$dtStart_orig=$this->date($date['datetime']);
			$dtStart_new=$this->date($dtStart_orig->format('Y-m-d ').sprintf('%02d',$d[$date['id']]['sh']+2).':'.sprintf('%02d',$d[$date['id']]['sm']).":00");
			$dtEnd_new=$this->date($dtStart_orig->format('Y-m-d ').sprintf('%02d',$d[$date['id']]['eh']).':'.sprintf('%02d',$d[$date['id']]['em']).":00");
			if($dtEnd_new<=$dtStart_new) return $this->response('Nesprávne zadaný čas. Odchod musí byť neskôr ako príchod',false);
			$secDiff=abs($dtStart_orig->format('U')-$dtStart_new->format('U'));
			$sql='';
			if((int)$secDiff>300){ // 300 second = 5 min - update record
				$error=$attdModel->updateRecords('dates',array('datetime'=>$dtStart_new->format('Y-m-d H:i:s')),'id='.$date['id']);
				if($error!==true)$errors[]=$error;
			}
			$insArr=array(
				'user_id'=>	$date['user_id'],
				'datetime'=>$dtEnd_new->format('Y-m-d H:i:s'),
				'way'=>1,
			);
			$error=$attdModel->insertRecords('dates',$insArr);
			if($error!==true)$errors[]=$error;
		}	
		return (!empty($errors))?$this->response('Nastala chyba: '.implode('<br>',$errors)):$this->response('Uložené');
	}

}
?>