<?php
namespace PodioBridge\Classes\Post;
use PodioBridge\Libs\ApiLib as ApiLib;
/**
*
*/
class Oberon extends ApiLib
{
	private $pass=PASSWORD;
	private $user=USERNAME;

	public function GetLoginSalt(){
		$salt=strtoupper($this->generateRandomString(40));
		if(file_put_contents('salt',$salt))
			$response=$this->oberonResponse($salt,true,"Salt returned");
		else
			$response=$this->oberonResponse(NULL,true,"Salt not saved");
		return $response;
	}
	public function LoginUser(){
		$data_json=$this->decodeJson();
		@$loginob=$data_json->LoginDataParameters;
		$salt=@file_get_contents('salt');

		//presunut do configu
		$this->pass=sha1($salt.$this->pass);

		//for debuging
		//file_put_contents('post.txt',$raw_data."\r\n",FILE_APPEND);
		//file_put_contents('post.txt',$pass."\r\n",FILE_APPEND);

		if($this->pass==@$loginob->Password && $this->user==@$loginob->UserName){
			$id=strtoupper($this->generateRandomString(40));
			if(file_put_contents("id",$id.PHP_EOL))
				$response=$this->oberonResponse($id,true,"Logged in");
			else
				$response=$this->oberonResponse(NULL,true,"ID not saved");
			@unlink("salt");
		}
		else{
			$response=$this->oberonResponse(NULL,false,"Wrong password",72100);
		}
		return $response;
	}

	/**
	 * Get all products marked as benefit from podio
	 * @return mixed podioCollection object of benefits or false
	 */
	private function getAllBenefits(){
		$podioApp=\PodioBridge\loadModel("podio.products");
		$tmp=$podioApp->filterItems(array("benefit"=>1));
		if($tmp->filtered>0)
			return $tmp;
		else
			return false;
	}

	/**
	 * Getting app ids from podio not supported
	 * @param  string $card_id real id of an card
	 * @return array          list of products as podio items
	 */
	private function getBenefitsByCard($card_id){
		return false;
	}

	private function getUserById($user_id){
		$podioApp=\PodioBridge\loadModel("podio.users");
		return $podioApp->getItemById($user_id);
	}	
	/**
	 * Get loyalty card from podio
	 * @param string  $cardId
	 * @param boolean $blank
	 * @param [type]  $podioCard_ob
	 */
	public function GetLoyaltyCard($cardId="",$blank=false,$podioCard_ob=null){
		$oberonCard_ob=\PodioBridge\loadModel('oberon.cards');
		$card_ob=$oberonCard_ob->getAllVars();

		if(!$this->checkLogin()){
			return $this->oberonResponse($card_ob,false,"Užívatel neprihlásený",2);
		}

		if(empty($cardId)){
			$data_json=$this->decodeJson();
			$cardId=$data_json->Number;
		}
		if(empty($cardId))
			return $this->oberonResponse($card_ob,false,"Id karty nezadané",11);
		if(empty($podioCard_ob)){
			$podioCardApp=\PodioBridge\loadModel("podio.cards");
			$podioCard_ob=$podioCardApp->getCardById($cardId);
		}
		if(empty($podioCard_ob))
			return $this->oberonResponse($card_ob,false,"Karta nenájdená",80);
		$podioUser_ob=$this->getUserById($podioCard_ob->fields['klient']->values[0]->item_id);
		$oberonCard_ob->serializePodioObject(array('cards'=>$podioCard_ob,'users'=>$podioUser_ob));
		switch((int)$oberonCard_ob->getVar('IsValid')){
			case 1: $oberonCard_ob->setVar('Status',1);$oberonCard_ob->setVar('IsValid',1);break;
			case 2: $oberonCard_ob->setVar('Status',0);$oberonCard_ob->setVar('IsValid',0);break;
			default: $oberonCard_ob->setVar('Status',-1);$oberonCard_ob->setVar('IsValid',0);break;
		}
		$card_ob=$oberonCard_ob->getAllVars();
		if($blank)
			return $card_ob;
		else
			return $this->oberonResponse($card_ob,true,"Card details sent");
	}
	/**
	 * Create or update loyalty card and if necessary create or update user
	 */
	public function SetLoyaltyCard(){
		if(!$this->checkLogin()){
			return $this->oberonResponse(NULL,false,"Užívatel neprihlásený",2);
		}
		$data_json=$this->decodeJson();
		$oberon_ob=$data_json->LoyaltyCard;

		//kontrola udajov
		// urobit kontrolu emailu
		if(empty($oberon_ob->Number))
			return $response=$this->oberonResponse(NULL,false,"Číslo karty musí byť zadané",80);
		if(empty($oberon_ob->Email))
			return $response=$this->oberonResponse(NULL,false,"Email musí byť zadaný",80);
		if(empty($oberon_ob->PersonFirstName) || empty($oberon_ob->PersonLastName))
			return $response=$this->oberonResponse(NULL,false,"Meno a priezvisko musí byť zadané",80);
		if(!filter_var($oberon_ob->Email, FILTER_VALIDATE_EMAIL))
			return $response=$this->oberonResponse(NULL,false,"Email má zlý formát",80);

		//user array
		$location="";
		$user_array=$bday=$acuisition=array();
		if(!empty($oberon_ob->AddressStreet)) $location.=$oberon_ob->AddressStreet.", ";
		if(!empty($oberon_ob->AddressPostCode)) $location.=$oberon_ob->AddressPostCode." ";
		$location.=$oberon_ob->AddressCity;
		if(!empty($this->stringToDate($oberon_ob->PersonDateOfBirth))) $bday=array("start"=>$this->stringToDate($oberon_ob->PersonDateOfBirth),"end"=>$this->stringToDate($oberon_ob->PersonDateOfBirth));
		if(!empty($this->stringToDate($oberon_ob->DateCreated))) array("start"=>$this->stringToDate($oberon_ob->DateCreated),"end"=>$this->stringToDate($oberon_ob->DateCreated));
		else $acuisition=$this->date(null,false,"UTC")->format("Y-m-d H:i:s");
		$user_array['text']['last-name']=$oberon_ob->PersonLastName;
		$user_array['text']['first-name']=$oberon_ob->PersonFirstName;
		$user_array['text']['titul']=$oberon_ob->PersonTitle;
		$user_array['text']['email']=$oberon_ob->Email;
		$user_array['text']['phone']=$oberon_ob->Phone;
		$user_array['text']['location']=$location;
		$user_array['date']['birthday']=$bday;
		$user_array['date']['day-of-aquisition']=$acuisition;
		if($oberon_ob->LoyaltyCardType==1)
			$user_array['category']['role']=1;
		if($oberon_ob->LoyaltyCardType==2)
			$user_array['category']['role']=4;
		$user_array['category']['acquisition']=5;
		$user_array['category']['povolenie-kontaktovat']=2;
		$user_array['category']['newsletter-status']=1;

		$podioUserApp=\PodioBridge\loadModel("podio.users");
		$podioUserApp->checkUserInPodio($user_array);
		if($podioUserApp->filtered==0){
			$user_ob=$podioUserApp->generateItemObject($user_array);
			$newUser=$podioUserApp->insertItem($user_ob);
			$userId=$newUser->item_id;
		}
		elseif($podioUserApp->filtered==1){
			$podioUserApp->updateItem($podioUserApp->user->item_id,$user_array);
			$userId=$podioUserApp->user->item_id;
		}
		else
			return $this->oberonResponse(NULL,false,"Identifikovaných viacero užívateľov",80);

		//card array
		$card_array=array();
		$card_array['app']['klient']=$userId;
		$card_array['category']['platnost']=1;
		$card_array['category']['typ-karty']=$oberon_ob->LoyaltyCardType;
		$card_array['money']['pocet-bodov']="0.000";
		$card_array['money']['hodnota-zakaznika']="0.000";
		$card_array['text']['id-v-pokladni-2']=$oberon_ob->Number;
		$card_array['date']['platnost-od-do']=array("start"=>$this->stringToDate($oberon_ob->DateCreated,true),"end"=>$this->stringToDate($oberon_ob->ValidToDate,true));
		$card_array['text']['poznamka']=$oberon_ob->Notice;

		$podioCardsApp=\PodioBridge\loadModel("podio.cards");
		$cardCheck=$podioCardsApp->filterItems(array('klient'=>$card_array['app']['klient']));
		if($cardCheck->filtered==0){
			$card_ob=$podioCardsApp->generateItemObject($card_array);
			$newCard=$podioCardsApp->insertItem($card_ob);
			$cardId=$newCard->item_id;
		}
		elseif($cardCheck->filtered==1){
			unset($card_array['date']['platnost-od-do'],$card_array['money']['pocet-bodov'],$card_array['money']['hodnota-zakaznika']);
			$podioCardsApp->updateItem($cardCheck[0]->item_id,$card_array);
			$cardId=$cardCheck[0]->item_id;
		}
		else
			return $this->oberonResponse(NULL,false,"Identifikovaných viacero zákazníckych kariet",80);

		if(!empty($userId) && !empty($cardId))
			return $this->oberonResponse($cardId,true,"Údaje uložené");
		return $this->oberonResponse(NULL,false,"Údaje nenačítané",80);
	}
	/**
	 * Sets transaction for loyalty card
	 */
	public function SetLoyaltyCardTransaction(){
		if(!$this->checkLogin()){
			return $this->oberonResponse(NULL,false,"Užívatel neprihlásený",2);
		}
		$data_json=$this->decodeJson();
		$oberon_ob=$data_json->LoyaltyCardTransaction;
		$sum=0;
		//odpoved na vybranie benefitu, nech je oberon stastny
		if((int)$oberon_ob->Type==35)return $this->oberonResponse(array("LoyaltyCardTotalPoints"=>0,"TransactionDateTime"=>$this->date()->format(OBERON_DATE_FORMAT),"TransactionID"=>0,"TransactionPoints"=>0),true,"Benefit evidovaný");
	// Created products if needed and save IDs for sale
		foreach($oberon_ob->StockItems as $item){
			$product_array=array();
			$product_array['text']['title']=$item->Name;
			$product_array['money']['price']=$item->PriceWithVATUnit;
			$product_array['text']['jednotka']=$item->Unit;
			$product_array['text']['ciarovy-kod']=$item->BarCode;
			$product_array['text']['id-v-pokladni']=$item->Number;
			$product_array['text']['description']=$item->Notice;
			$product_array['text']['benefit']=2;
			$product_array['text']['stav']=1;

			$podioProductApp=\PodioBridge\loadModel("podio.products");
			$podioProductApp->filterItems(array('id-v-pokladni'=>$product_array['text']['id-v-pokladni']));
			if($podioProductApp->filtered==0){
				$product_ob=$podioProductApp->generateItemObject($product_array);
				$newProduct=$podioProductApp->insertItem($product_ob);
				$productId=$newProduct->item_id;
			}
			elseif($productCheck->filtered==1){
				$this->updateItem($productCheck[0]->item_id,$product_array);
				$productId=$productCheck[0]->item_id;
			}
			else
				return $this->oberonResponse(NULL,false,"Identifikovaných viacero zákazníckych kariet",80);
			$product_ids[]=$productId;
			$sum=$sum+$item->PriceWithVATUnit*$item->Amount;
		}
	// Update loyalty card		
		$podioCardsApp=\PodioBridge\loadModel("podio.cards");
		$podioCard_ob=$podioCardsApp->getCardById($oberon_ob->CardNumber);
		if($podioCard_ob===false) return $this->oberonResponse(NULL,false,"Karta sa nenašla",80);
		$card_array=array();
		if($sum>0){
			$customerValue=$podioCard_ob->fields['hodnota-zakaznika']->amount+$sum;
			$card_array['money']['hodnota-zakaznika']=array("value"=>$customerValue,"currency"=>"EUR");
		}
		elseif($sum<0){
			$oberon_ob->Type=35;
			$userPoints=$this->getPodioFieldValue($podioCard_ob->item_id,85398276,'cards');
			if($userPoints<$oberon_ob->TypeValue)
				return $this->oberonResponse(NULL,false,"Nedostatočný počet bodov",80);
		}
		switch((int)$oberon_ob->Type){
			case 11: $type=1;$sum=0;break;
			case 12: $type=2;$sum=0;break;
			case 14: $type=3; break;
			case 15: $type=4; break;
			case 25: $type=5; break;
			case 35: $type=6; break;
		}
		$userPoints=$podioCard_ob->fields['pocet-bodov']->amount+$sum;
		$card_array['money']['pocet-bodov']=array("value"=>round($userPoints),"currency"=>"EUR");
		$podioCardsApp->updateItem($podioCard_ob->item_id,$card_array);

	// Create sale
		$podioSalesApp=\PodioBridge\loadModel("podio.sales");
		$sale_array=$closingTime=array();
		if(!empty($this->stringToDate($oberon_ob->DateTime,true))) array("start"=>$this->stringToDate($oberon_ob->DateTime,true),"end"=>$this->stringToDate($oberon_ob->DateTime,true));
		else $closingTime=$this->date(null,false,"UTC")->format("Y-m-d H:i:s");
		$sale_array['app']['zakaznicka-karta-2']=(int)$podioCard_ob->item_id;
		$sale_array['app']['product']=$product_ids;
		$sale_array['text']['suma-pradaju']=$sum;
		$sale_array['date']['closing-time']=$closingTime;
		$sale_array['text']['notes']=$oberon_ob->Notice;
		$sale_array['text']['identifikator']=$oberon_ob->Identifier;
		$sale_array['text']['agent-2']=1;
		$sale_array['text']['id-zdrojovej-evidencie']=(string)$oberon_ob->SourceEvidenceIDNum;
		$sale_array['text']['miesto-transakcie']=(string)$oberon_ob->TransactionPlace;
		$sale_array['category']['typ-transakcie-2']=$type;
		$sale_ob=$podioSalesApp->generateItemObject($sale_array);
		$newSale=$podioSalesApp->insertItem($sale_ob);
		$saleId=$newSale->item_id;

		if(!empty($saleId)){
			return $this->oberonResponse(array("LoyaltyCardTotalPoints"=>round($userPoints),"TransactionDateTime"=>$this->date()->format(OBERON_DATE_FORMAT),"TransactionID"=>$saleId,"TransactionPoints"=>round($sum)),true,"Predaj uložený");
		}
		return $this->oberonResponse(NULL,false,"Predaj neuložený",80);
	}
	/**
	 * TODO fix the function
	 */
	public function GetLoyaltyCardTransactionInfo(){
		if(!$this->checkLogin()) return $this->oberonResponse(NULL,false,"Užívatel neprihlásený",2);
		$oberon_ob=$this->decodeJson();
		if(empty($oberon_ob->Number)) return $this->oberonResponse(NULL,false,"Nezadané číslo",80);
		$podioCardsApp=\PodioBridge\loadModel("podio.cards");
		$podioCard_ob=$podioCardsApp->getCardById($oberon_ob->Number);
		if(empty($podioCard_ob)){
			return $this->oberonResponse(NULL,false,"Karta nenačítaná",80);
		}
		$podioSalesApp=\PodioBridge\loadModel("podio.sales");
		$podioSales_ob=$podioSalesApp->getSalesByCardId($podioCard_ob->item_id);

		foreach($podioSales_ob as $sale){
			$oberonSale_ob=\PodioBridge\loadModel('oberon.sales');
			$oberonSale_ob->serializePodioObject(array('sales'=>$sale));
			$card_number=$oberonSale_ob->getVar('CardNumber');
			if(is_array($card_number) && count($card_number)!=1)
				return $this->oberonResponse(NULL,false,"Chyba pri načítaní kariet",80);
			else
				$oberonSale_ob->setVar('CardNumber',$card_number[0]);
			switch((int)$oberonSale_ob->getVar('Type')){
				case 1: $type=11;break;
				case 2: $type=12;break;
				case 3: $type=14; break;
				case 4: $type=15; break;
				case 5: $type=25; break;
				case 6: $type=35; break;
				default: $type=0; break;
			}
			$oberonSale_ob->setVar('Type',$type);

			$oberonSales_array[]=$oberonSale_ob->getAllVars();
		}
		$cards_num=count($oberonSales_array);
		$response=new stdClass();
		$response->Description="Sent $cards_num sales";
		$response->LoyaltyCard=$this->GetLoyaltyCard($oberon_ob->Number,true,$podioCard_ob);
		$response->TransactionList=$oberonSales_array;

		return $this->oberonResponse($response,true,"Transakcie odoslané");
	}	

	public function GetBenefits(){
		//return $this->oberonResponse(NULL,false,"Benefity sa zatiaľ nepoužívajú",80);
		if(!$this->checkLogin()){
			return $this->oberonResponse(NULL,false,"Užívatel neprihlásený",2);
		}
		$oberon_ob=$this->decodeJson();
		$oberonProduct_ob=\PodioBridge\loadModel('oberon.products');
		if($oberon_ob->OnlyAvailable)
			$podioProducts_ob=$this->getBenefitsByCard($oberon_ob->Number);
		else
			$podioProducts_ob=$this->getAllBenefits();
		if(empty($podioProducts_ob))
			return $this->oberonResponse($oberonProduct_ob,false,"Benefity nenačítané",80);
		foreach($podioProducts_ob as $podioProduct_ob){
			$oberonProduct_ob=\PodioBridge\loadModel('oberon.products');
			$oberonProduct_ob->serializePodioObject(array('products'=>$podioProduct_ob));
			$oberonProduct_ob->setVar('Amount',1);
			switch((int)$oberonProduct_ob->getVar('IsValid')){
				case 1: $oberonProduct_ob->setVar('IsValid',1);break;
				case 2: $oberonProduct_ob->setVar('IsValid',0);break;
				default: $oberonProduct_ob->setVar('IsValid',0);break;
			}
			switch((int)$oberonProduct_ob->getVar('StockItemAcceptanceType')){
				case 1: $oberonProduct_ob->setVar('StockItemAcceptanceType',0);break;
				case 2: $oberonProduct_ob->setVar('StockItemAcceptanceType',11);break;
				case 3: $oberonProduct_ob->setVar('StockItemAcceptanceType',13);break;
				case 4: $oberonProduct_ob->setVar('StockItemAcceptanceType',14);break;
				default: $oberonProduct_ob->setVar('StockItemAcceptanceType',0);break;
			}
			$oberonProducts_array[]=$oberonProduct_ob->getAllVars();
		}
		return $this->oberonResponse($oberonProducts_array,true,"Benefity načítané");
	}
}
?>