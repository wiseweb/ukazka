<?php
namespace PodioBridge\Classes\Post;
use PodioBridge\Libs\ApiLib as ApiLib;

/**
*
*/
class Podio extends ApiLib
{
	public function podioAPI(){
		$type=array_shift($this->commands);
		$podioApp=\PodioBridge\loadModel("podio.".$type);
		$podio_methods=array('item.create','item.update','item.delete');
		if($_POST['type']=='hook.verify'){
			$validation=$podioApp->validateHook($_POST['hook_id'], $_POST['code']);
		}
		elseif(in_array($_POST['type'],$podio_methods)){
			if(method_exists($this, $this->commands[0]) && !empty($this->commands[0])) {
				$command=array_shift($this->commands);
				$response=$this->{$command}();
				return $this->response($response["msg"],$response["status"]);
			}
			else
				return $this->response("Wrong command",false);
		}
		else return $this->response("Wrong event",false);
	}
	public function sendContentToBuffer(){
		$social=false;
		$podioApp=\PodioBridge\loadModel("podio.content");
		$item=$podioApp->getItemById($_POST['item_id']);
		foreach($item->fields["type-of-annct"]->values as $type){
			if($type["id"]==5) $social=true;
		}
		if($social==true && $item->fields["status"]->values[0]["id"]==4 && $item->fields["zdielane"]->values[0]["id"]!=2){
			$bufferApp=\PodioBridge\loadModel("buffer.buffer");
			$data=array();
			if(empty($item->fields['datum-publikacie'])){
				$date = new \DateTime("now",new \DateTimeZone("Europe/Bratislava"));
				$date->setTimeZone(new \DateTimeZone("UTC"));
				$item->fields['datum-publikacie']=new \PodioDateItemField();
				$item->fields['datum-publikacie']->values=array("start"=>$date->format("Y-m-d H:i:s"),"end"=>$date->format("Y-m-d H:i:s"));
			}
			else $data["scheduled_at"]=$item->fields['datum-publikacie']->start->format('U');
			$text=empty($item->fields["results"]->values)?$bufferApp->cleanBufferText($item->fields["about-the-release-top-key-messages"]->values):$bufferApp->cleanBufferText($item->fields["about-the-release-top-key-messages"]->values."\r\n\r\n".$item->fields["results"]->values);
			$data["text"]=$text;
			$data["top"]=($item->fields["priority"]->values[0]["id"]==2)?true:false;
			$data["now"]=($item->fields["now"]->values[0]["id"]==2)?true:false;
			$media=array();
			if($item->fields["link"]->values)
				$media["link"]=$bufferApp->cleanBufferText($item->fields["link"]->values[0]->original_url);
			if($item->fields["images"]->values){
				if(empty($item->fields["pridat-vodotlac"]->values))
					$media["picture"]=$media["thumbnail"]=$media["photo"]=$this->GetImage($item->fields["images"]->values[0]->file_id,true);
				else
					$media["picture"]=$media["thumbnail"]=$media["photo"]=$this->GetImage($item->fields["images"]->values[0]->file_id,true,"podio.content",$item->fields["pridat-vodotlac"]->values[0]["id"]);
			}
			if(!empty($media))
				$data["media"]=$media;
			$return=$bufferApp->setBufferPost($data);
			if((bool)$return->success){
				$notification="Pridané na:".PHP_EOL;
				foreach($return->updates as $profile){
					$date = new \DateTime();
					$date->setTimestamp($profile->due_at);
					$date->setTimeZone(new \DateTimeZone("Europe/Bratislava"));
					$notification.=ucfirst($profile->profile_service)." - ".$date->format("d.m.Y \o H:i:s").PHP_EOL;
				}
				if($return->buffer_percentage==100)
					$notification.="**Buffer plný!**";
				$podioApp->commentOnItem($item->item_id,  $notification);
				unset($item->fields['zdielane']);
				$item->fields['zdielane']=new \PodioCategoryItemField(array("field_id" => 'zdielane', "values" => 2));
				$item->save($this->options);
			}
			else $podioApp->commentOnItem($item->item_id,  $return->message);

		}
	}
	/* DANGEROUS ONE TIME FUNCTIONS TODO fix for api v2
	public function update2(){
		$this->filterAllItems(array('role'=>array( 3,1 )),'users');
		$x=1;
		foreach($this->items as $users){
			foreach($users as $user){
				unset($user->fields['povolenie-kontaktovat']);
				$user->fields["povolenie-kontaktovat"]=new PodioCategoryItemField(array("field_id" => 'povolenie-kontaktovat', "values" => 2));
				$user->save($this->options);
				$x++;
			}
		}
		file_put_contents("a.log", $x);
	}

	public function update(){
		$dt=new DateTime();
		$users=$this->getAllItems('users');
		$x=1;
			foreach($users as $user){
				if(empty($user->fields['role']));
				$user->fields["role"]=new PodioCategoryItemField(array("field_id" => 'role', "values" => 1));
				$user->save($this->options);

				$x++;
			}
				file_put_contents("a.log", $x);
	}*/
	public function createHook(){
		$podioApp=\PodioBridge\loadModel("podio.content");
		$field_id=array_shift($this->commands);
		$event_type = "item.update"; // Only act when field values are updated
		$hook_id=$podioApp->createFieldHook($field_id, "http://podiobridge.vitaverde.sk/api/podio/podioApi/content/sendContentToBuffer", $event_type, 'campaigns');
		$podioApp->verifyHook($hook_id);
		return $this->response($hook_id,true);
   }
	public function deleteHook(){
		$podioApp=\PodioBridge\loadModel("podio.content");
		$id=array_shift($this->commands);
		$hooks = $podioApp->deleteHook($id);
		 return $this->response("Done",true);
	}


}
?>
