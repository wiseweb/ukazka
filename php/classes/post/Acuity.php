<?php
namespace PodioBridge\Classes\Post;
use PodioBridge\Libs\ApiLib as ApiLib;
/**
*
*/
class Acuity extends ApiLib
{
	/**
	 * Send sms notification to customer about service. Saves/updates customer if needed
	 * @return object response
	 */
	public function notify(){
		$acuityApp=\PodioBridge\loadModel("acuity.acuity");
		$body = file_get_contents('php://input');
		// Check if request comming from Acuity
		$acuityApp->verifyMessageSignature(ACUITY_PASS, $body);
		parse_str($body);
		$data = $acuityApp->request('/appointments/'.$id);
		//$data = json_decode($result, true);
		//user array for podio insert
		$mobile=parent::sanitizeMobile($data["phone"]);
		$user_array=array();
		$user_array['text']['last-name']=$data['lastName'];
		$user_array['text']['first-name']=$data['firstName'];
		$user_array['text']['email']=$data['email'];
		$user_array['text']['phone']=$mobile;
		$dt=$this->date($data["datetime"]);
		$user_array['date']['day-of-aquisition']=array("start"=>$this->now->format("Y-m-d H:i:s"),"end"=>$this->now->format("Y-m-d H:i:s"));
		$user_array['date']['posledna-navsteva']=array("start"=>$dt->format("Y-m-d H:i:s"),"end"=>$dt->format("Y-m-d H:i:s"));
		$user_array['money']['hodnota-zakaznika']=$data["price"];
		$user_array['category']['role']=1;
		$user_array['category']['acquisition']=4;
		$user_array['category']['povolenie-kontaktovat']=2;
		$user_array['category']['newsletter-status']=1;
		// Insert to podio
		switch($action){
			case 'rescheduled':$status="Zmenene";break;
			case 'canceled':$status="Zrusene";break;
			default:$status="Potvrdene";break;
		}
		$podioApp=\PodioBridge\loadModel("podio.users");
		$podioApp->checkUserInPodio($user_array);
		if($podioApp->filtered==0){
			$user_ob=$podioApp->generateItemObject($user_array);
			$podioApp->user=$podioApp->insertItem($user_ob);
		}
		elseif($podioApp->filtered==1){
			// update to match data from acuity
			if(strlen($podioApp->user->fields['last-name']->values)<strlen($user_array['text']['last-name']))
				$podioApp->user->fields['last-name']=new \PodioTextItemField(array("field_id" => 'last-name', "values" => $user_array['text']['last-name']));
			if(strlen($podioApp->user->fields['first-name']->values)<strlen($user_array['text']['first-name']))
				$podioApp->user->fields['first-name']=new \PodioTextItemField(array("field_id" => 'first-name', "values" => $user_array['text']['first-name']));
			if(empty($podioApp->user->fields['email']->values))
				$podioApp->user->fields['email']=new \PodioTextItemField(array("field_id" => 'email', "values" => $user_array['text']['email']));
			if($podioApp->user->fields['phone']->values!=$mobile && !empty($mobile))
				$podioApp->user->fields['phone']=new \PodioTextItemField(array("field_id" => 'phone', "values" => $user_array['text']['phone']));
			elseif($podioApp->user->fields['phone']->values!=$mobile && empty($mobile))
				$mobile=$podioApp->user->fields['phone']->values;
			foreach($podioApp->user->fields['role']->values as $key=>$roles){
			if($role['id']==7 || $role['id']==3)
				 unset($podioApp->user->fields['role']->values[$key]);
			}
			// if we have no phone in acuity, but we have one in Podio, use that
			$podioApp->user->fields['role']=new \PodioCategoryItemField(array("field_id" => 'role', "values" => $user_array['category']['role']));
			if(empty($podioApp->user->fields['posledna-navsteva']))$podioApp->user->fields['posledna-navsteva']=new \PodioDateItemField();
			$podioApp->user->fields['posledna-navsteva']->values=$user_array['date']['posledna-navsteva'];
			$value=$podioApp->user->fields['hodnota-zakaznika']->values['value'];
			if($action=="scheduled"){
				if(empty($value))
					$podioApp->user->fields['hodnota-zakaznika']=new \PodioMoneyItemField(array("external_id" => 'hodnota-zakaznika', "values" => array("value"=>(float)$data["price"],"currency"=>"EUR" )));
				else
					$podioApp->user->fields['hodnota-zakaznika']->amount=$value+$data["price"];
			}
			if($action=="canceled"){
				if(empty($value) || $value<0)
					$podioApp->user->fields['hodnota-zakaznika']=new \PodioMoneyItemField(array("external_id" => 'hodnota-zakaznika', "values" => array("value"=>0,"currency"=>"EUR" )));
				else
					$podioApp->user->fields['hodnota-zakaznika']->amount=$value-$data["price"];
			}
			$podioApp->user->save();
		}
		// Send an sms
		if(!empty($mobile)){
			$not_limit=clone $dt;
			$not_limit->modify("-30 minutes");
			if($this->now<$not_limit){
				if($this->checkMobile($mobile)){
					$footer=" Vase Vita Verde. Ak je to omyl, prosim kontaktujte nas na tel. 0948134909";
					$service =$this->convertAccents($data['type']);
					//append service details if required
					$trans = array("január" => "january", "február" => "february", "marec" => "march", "apríl" => "april", "máj" => "may", "jún" => "jun", "júl" => "july", "august" => "august", "september" => "september", "otóber" => "october", "november" => "november", "december" => "december");
					$body = $status.". ";
					$desclen=30;
					if($desclen && strlen($service)<=$desclen)
						$body .= $service;
					else{
						$part=@substr($service,0, @strpos($service, ' ', $desclen));
						if(empty($part) || !$part) $body.= $service;
						else $body.=$part."...";
					}
					//(strlen($service)>50)?$body .= substr($service,0,20).'... ...'.substr($service,strlen($service)-20,20):$body.=$service;
					$body.=" dna ";
					$body.= $this->date(strtr($data['date'],$trans)." ".$data['time'])->format('j.n.Y \o H:i');
					$body.='.'.$footer;
					if($this->sendSMS($mobile, $body))  $notification="SMS zaslaná: ".$mobile;
					else  $notification= 'Nepodarila sa zaslať SMS.';

				}
				else $notification="Zlé číslo: ".$mobile;
			}
			else $notification="Zmena v minulosti";
		}
		else $notification="Bez čísla";
		$not="Objednavka;".$status.";".$dt->format("H:i  d.m.Y").";".$data["type"].";".$notification;
		if(!empty($podioApp->user->item_id)) $podioApp->commentOnItem($podioApp->user->item_id, $not);
		else $not="Error saving to Podio: ".$notification;
		return $this->response($not);
	}

}
?>