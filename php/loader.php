<?php
namespace PodioBridge;

function load($namespace) {
	$splitpath = explode('\\', $namespace);
	$path = '';
	$name = '';
	$firstword = true;
	for ($i = 0; $i < count($splitpath); $i++) {
		if ($splitpath[$i] && !$firstword) {
			if ($i == count($splitpath) - 1)
				$name = $splitpath[$i];
			else
				$path .= DS . $splitpath[$i];
		}
		if ($splitpath[$i] && $firstword) {
			if ($splitpath[$i] != __NAMESPACE__)
				break;
			$firstword = false;
		}
	}
	if (!$firstword) {
		$fullpath = __DIR__ . strtolower($path) . DS . $name . '.php';
		if(!file_exists($fullpath)) die("Wrong class ".$name);
		return include_once($fullpath);
	}
	return false;
}
/**
 * load needed APIs from models
 * @param  string $appType clean type of app
 * @return object          podio app
 */
function loadModel($appType){
	if(empty($appType))
		return false;
	$file=explode(".", $appType);
	$file=array_map(function($word) { return ucfirst($word); }, $file);
	$path=implode("\\",$file);
	$className='\PodioBridge\Models\\'.$path;
	return new $className;
}

/**
 * CURL get webpage data (for sendSMS function and acuity)
 * @param string $url clean url
 * @param string $data clean data string
 * @return mixed      content of website
 */
function get_web_page($url,$data=NULL) {
	$options = array(
			CURLOPT_RETURNTRANSFER => true,   // return web page
			CURLOPT_HEADER         => false,  // don't return headers
			CURLOPT_FOLLOWLOCATION => true,   // follow redirects
			CURLOPT_MAXREDIRS      => 10,     // stop after 10 redirects
			CURLOPT_ENCODING       => "",     // handle compressed
			CURLOPT_AUTOREFERER    => true,   // set referrer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,    // time-out on connect
			CURLOPT_TIMEOUT        => 120,    // time-out on response
	);
	if(!empty($data)){
		$options[CURLOPT_POST]= 1;
		$options[CURLOPT_POSTFIELDS]= $data;
	}
	$ch = curl_init($url);
	curl_setopt_array($ch, $options);
	$content  = curl_exec($ch);
	curl_close($ch);
	return $content;
}

function loadPath($absPath) {
	return include_once($absPath);
}

spl_autoload_register(__NAMESPACE__ . '\load');

?>